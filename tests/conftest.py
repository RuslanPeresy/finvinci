import sys

sys.path.append('..')

from exchange import app as main_app, session, Session, engine
import pytest
from exchange.models import Base


@pytest.yield_fixture(scope='function')
def app():
    _app = main_app

    Base.metadata.create_all(bind=engine)
    _app.connection = engine.connect()

    yield _app

    #_app.connection.close()
    #Session.close_all()
    #Base.metadata.drop_all(bind=engine)


@pytest.fixture
def client(app):
    return app.test_client()
