import sys

sys.path.append('..')


def test_converter(client):

  result = client.post('/convert', json={
      'base_currency': 'USD', 'convertial_currency': 'EUR', 'base_amount': 1.0, })
  assert result.status_code == 200

  assert '0.9' in result.get_json()['converted_amount']


def test_error(client):
  bad_res = client.post('/convert', json={
      'base_currency': 123
  })
  assert bad_res.status_code == 400
  assert bad_res.get_json()['message'] == {
      'base_amount': ['Missing data for required field.'],
      'base_currency': ['Not a valid string.'],
      'convertial_currency': ['Missing data for required field.']}
