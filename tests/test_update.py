from exchange.tasks import update_rates


def test_periodic():
    task = update_rates.apply_async()
    # it takes 7 minutes because of
    # fcsapi.com API requests limits
    assert task.get() == 'Updated'
