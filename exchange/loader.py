import requests
from exchange.models import Rate
from exchange import session
import time

url = 'https://fcsapi.com/api-v2/forex/latest?access_key=KsQ8SdyaQUwyIdoERVaoceaEKV1xIYSwl3PHCVHUEIlKXHN'

curs = ['USD', 'EUR', 'PLN', 'CZK']


class Loader:
    def update(self):
        for idx, cur in enumerate(curs):
            to_convert = curs.copy()
            base = to_convert.pop(idx)
            for convertial_cur in to_convert:
                convert_rate = self.fetch_data(base, convertial_cur)
                rate = Rate.get(cur)
                setattr(rate, convertial_cur, convert_rate)
                session.commit()
                time.sleep(20)  # to don't overcome API requests limit

    @staticmethod
    def fetch_data(base, convertial_cur):
        res = requests.get(
            url, params={'symbol': f'{base}/{convertial_cur}'})
        result = res.json()['response'][0]['price']
        return result
