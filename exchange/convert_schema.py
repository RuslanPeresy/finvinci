from exchange import ma
from marshmallow import fields, validate, pre_load


class ConvertSchema(ma.Schema):
    base_currency = fields.String(
        required=True,
        validate=[validate.OneOf([
            'EUR', 'USD', 'CZK', 'PLN'])])
    convertial_currency = fields.String(
        required=True,
        validate=[validate.OneOf([
            'EUR', 'USD', 'CZK', 'PLN'])])
    base_amount = fields.Decimal(
        required=True)
    converted_amount = fields.Decimal(
        dump_only=True,
        as_string=True)

    """@pre_load
    def preprocess_amount(self, data, **kwargs):
        data['base_amount'] = str(data['base_amount'])"""
