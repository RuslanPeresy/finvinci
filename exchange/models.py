from exchange import db, session, ma
from sqlalchemy.ext.declarative import declarative_base
from marshmallow import fields, validate


Base = declarative_base()


class Rate(Base):
    __tablename__ = 'rates'
    id = db.Column(db.Integer, primary_key=True)
    USD = db.Column(db.DECIMAL(), nullable=False)
    EUR = db.Column(db.DECIMAL(), nullable=False)
    PLN = db.Column(db.DECIMAL(), nullable=False)
    CZK = db.Column(db.DECIMAL(), nullable=False)
    base = db.Column(db.String(), nullable=False)

    def __init__(self, base, rate_data):
        self.base = base
        for key, value in rate_data.items():
            setattr(self, key, value)

    @classmethod
    def get(cls, base):
        rate = session.query(cls).filter_by(base=base).first()
        return rate
