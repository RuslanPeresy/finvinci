import sqlalchemy as db
from flask import Flask
from flask_marshmallow import Marshmallow
from apispec import APISpec
from flask_apispec.extension import FlaskApiSpec
from apispec.ext.marshmallow import MarshmallowPlugin
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from flask_cors import CORS
from .config import Config
from decimal import Decimal
from celery import Celery


app = Flask(__name__)
app.config.from_object(Config)
ma = Marshmallow(app)
docs = FlaskApiSpec(app)
cors = CORS(app)


app.config.update(
    {
        "APISPEC_SPEC": APISpec(
            title="exchage",
            version="v1",
            openapi_version="2.0",
            plugins=[MarshmallowPlugin()],
        ),
        "APISPEC_SWAGGER_URL": "/swagger/",
    })

engine = create_engine(Config.SQLALCHEMY_DATABASE_URI)
Session = sessionmaker(bind=engine)
session = Session()


def make_celery():
    celery = Celery(__name__,
                    broker='redis://',
                    backend='redis://')
    TaskBase = celery.Task

    class ContextTask(TaskBase):
        abstract = True

        def __call__(self, *args, **kwargs):
            with app.app_context():
                return TaskBase.__call__(self, *args, **kwargs)

    celery.Task = ContextTask
    return celery


class Converter:

    def __init__(self, base_currency,
                 convertial_currency, base_amount):
        self.converted_amount = self.calculate(
            base_currency,
            convertial_currency,
            base_amount)

    @staticmethod
    def calculate(
            base_currency,
            convertial_currency,
            base_amount,
            multiplicity=1):
        rate_list = session.query(Rate).filter_by(
            base=base_currency).first()
        rate = getattr(rate_list, convertial_currency)

        return Decimal('{:.2f}'.format(
            base_amount * rate / multiplicity))


from . import routes


initial_data = {
    'EUR': {
        'EUR': Decimal(1),
        'USD': Decimal('1.09'),
        'CZK': Decimal('25.25'),
        'PLN': Decimal('4.3')
    },
    'USD': {
        'USD': Decimal(1),
        'EUR': Decimal('0.92'),
        'CZK': Decimal('23.24'),
        'PLN': Decimal('3.96')
    },
    'CZK': {
        'CZK': Decimal(1),
        'EUR': Decimal('0.04'),
        'USD': Decimal('0.043'),
        'PLN': Decimal('0.17')
    },
    'PLN': {
        'PLN': Decimal(1),
        'USD': Decimal('0.25'),
        'EUR': Decimal('0.23'),
        'CZK': Decimal('5.87')
    }
}

curs = ('EUR', 'USD', 'CZK', 'PLN')
from .models import Rate, Base

Base.metadata.create_all(bind=engine)


@app.before_first_request
def init_rates():
    for cur in curs:
        if not session.query(Rate).filter_by(base=cur).first():
            rate = Rate(cur, initial_data[cur])
            session.add(rate)
            session.commit()
