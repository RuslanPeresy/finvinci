from . import make_celery
from .loader import Loader

celery = make_celery()


@celery.task
def update_rates():
    loader = Loader()
    loader.update()
    return 'Updated'


@celery.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):
    sender.add_periodic_task(86400.0, update_rates,
                             name='update_every_24')
