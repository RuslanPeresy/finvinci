from flask import jsonify
from flask_apispec import use_kwargs, marshal_with
from exchange import app, docs, Converter
from .convert_schema import ConvertSchema


@app.route('/convert', methods=['POST'])
@use_kwargs(ConvertSchema)
@marshal_with(ConvertSchema(only=('converted_amount',)))
def convert_cur(**kwargs):
    try:
        converter = Converter(
            kwargs.get('base_currency'),
            kwargs.get('convertial_currency'),
            kwargs.get('base_amount')
        )
        return converter
    except Exception as e:
        return {'message': str(e)}, 400


@app.errorhandler(422)
def handle_error(err):
    headers = err.data.get("headers", None)
    messages = err.data.get("messages", ["Invalid request."])
    if headers:
        return jsonify({"message": messages}), 400, headers
    else:
        return jsonify({"message": messages}), 400


docs.register(convert_cur)
