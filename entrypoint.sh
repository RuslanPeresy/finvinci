#!/bin/bash
gunicorn -w 1 -b 0.0.0.0:5000 run:app --daemon
celery worker -l info -A exchange.tasks --logfile=log/celery.exchange.log --detach
celery -A exchange.tasks beat -l info --logfile=log/celery.beat.log --detach
celery flower -A exchange.tasks --address=0.0.0.0 --port=5555
