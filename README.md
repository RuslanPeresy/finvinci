# README #

### Setup ###

* Backend:

* Requires: Python 3.8, pipenv and running redis server (for exchange rates periodic updates)
* pipenv install

* Frontend:

* Tested with Node.js 12.13.1 and npm 6.13.6
* cd frontend
* npm install

### Run ###

* Backend:

* In Linux command line:
* pipenv shell
* ./entrypoint.sh

* Frontend:

* npm run serve (or npm run build & npm start)
* Go to http:localhost:8080

### Test ###

* coverage run -m pytest tests/test_endpoint.py tests/test_update.py
* NOTE: test_update.py takes 7 minutes to finish because of 
* fcsapi.com API requests per minute limits